﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2_Console
{
    class Program
    {
        //private static int select;
        
        static void Main(string[] args)
        {
          

              Console.WriteLine("+-------------------------------------------------------+");
              Console.WriteLine("|           Welcome to Your Shopping Online!            |");
              Console.WriteLine("+-------------------------------------------------------+");
  
            const int QUIT = 0;
            string x;
            double inputInt = 0, tempint = 0;
            do
            {
                Console.Write("Enter the amount to be add (type 0 to calculate and exit):\n");
                x = Console.ReadLine();
                Console.WriteLine("+------------------------------------------------------+");
                bool inputBool = double.TryParse(x, out tempint);

                if (inputBool == true)
                {
                    inputInt += tempint * 1.15;
                }

            } while (tempint != QUIT);

            Console.WriteLine("+---------------------------------------------------------+");
            Console.WriteLine("|                   Calculating...                        |");
            Console.WriteLine("+---------------------------------------------------------+");
          
            Console.WriteLine("            Adding and multiplying 15% GST...              ");
            Console.WriteLine("___________________________________________________________");
            Console.WriteLine("\n");
            Console.WriteLine("  Total is : ${0} to pay!!", inputInt);
            
            Console.WriteLine("+---------------------------------------------------------+");

            Console.ReadLine();
        }
        
    }
}
