﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task1_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btlKm_Click(object sender, RoutedEventArgs e)
        {
            var convert = textBox1.Text;
            lblResultTxt.Text = $"{convert} Miles is {convertToMiles(convert)} Km's";
        }
        static double convertToMiles(string input)
        {
            const double Miles2Km = 1.609344;
            var userNum = double.Parse(input);
            var answer = userNum * Miles2Km;

            return answer;
        }

        private void btlMiles_Click(object sender, RoutedEventArgs e)
        {
            var convert2 = textBox1.Text;
            lblResultTxt.Text = $"{convert2} Km's is {convertToKm(convert2)} Miles";
        }
        static double convertToKm(string input)
        {
            const double Km2Miles = 0.62137119;
            var userNum = double.Parse(input);
            var answer2 = userNum * Km2Miles;

            return answer2;
        }

        
    }
}
