﻿namespace Task_3_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showContent = new System.Windows.Forms.Button();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblShowName = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // showContent
            // 
            this.showContent.Location = new System.Drawing.Point(117, 131);
            this.showContent.Name = "showContent";
            this.showContent.Size = new System.Drawing.Size(75, 23);
            this.showContent.TabIndex = 0;
            this.showContent.Text = "check list\r\n";
            this.showContent.UseVisualStyleBackColor = true;
            this.showContent.Click += new System.EventHandler(this.showContent_clik);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeader.Location = new System.Drawing.Point(111, 13);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(202, 22);
            this.lblHeader.TabIndex = 1;
            this.lblHeader.Text = "Choose from the list :";
            // 
            // lblShowName
            // 
            this.lblShowName.AutoSize = true;
            this.lblShowName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShowName.Location = new System.Drawing.Point(114, 228);
            this.lblShowName.Name = "lblShowName";
            this.lblShowName.Size = new System.Drawing.Size(0, 22);
            this.lblShowName.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "carrot ",
            "potato",
            "parsnip",
            "pineaple",
            "banana",
            "lemon",
            "none"});
            this.comboBox1.Location = new System.Drawing.Point(117, 86);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 297);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lblShowName);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.showContent);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button showContent;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblShowName;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

