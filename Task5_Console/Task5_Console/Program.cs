﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5_Console
{
    class Program
    {
        //declare Global Methods and types/variables
        public static int SelectNumber = 0;
        public static Random rand = new Random();
        public static bool GameOver = false;
        static int counter = 0;
        static int score = 0;
        //declare main class/method 
        static void Main(string[] args)
        {
            //Variables SelectNumber for the random number and userNumber for the User
            SelectNumber = rand.Next(1, 5);
            int userNumber = 0;
            //Loop to give a condition for the User to enter a number between 1 and 5
            do
            {
                counter++;
                Console.WriteLine("Please enter a number between 1 and 5 :");
                userNumber = Convert.ToInt32(Console.ReadLine());
                //Call Method inside the main 
                GuessNumber(userNumber);
            }
            //Condition for the user if declare false continue or if declare true quit!
            while (GameOver == false);
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("Thanks for using this App !!\n Press any Key to Quit !");
            Console.WriteLine("------------------------------------------------------");
            Console.ReadLine();
        }
        //Declare Method void (no value returned)
        public static void GuessNumber(int userNumber)
        {
            
            int playAgain = 0;
            //condition for the userNumber input 
            if(userNumber <1 || userNumber >5)
            {
                Console.WriteLine("Please enter a valid number between 1 and 5 only!\n");
            }
            else if(userNumber < SelectNumber)
                Console.WriteLine("Too low. Try again !\n");
            
            else if(userNumber > SelectNumber)
                Console.WriteLine("Too high. Try again !\n");
            
            else
            {
             
                score++;
                Console.WriteLine(" You got it!! And you score 1 point !!");
                Console.WriteLine("\n Attempts:" + counter);
                Console.WriteLine("\n Score:"+ score);
                Console.WriteLine("------------------------------------------");
                Console.WriteLine("  Do you want to try again ? (1.yes/2.no) ");
                playAgain = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("------------------------------------------");
                Console.Clear();
                //Loop for the user choose to continue or quit
                while(playAgain != 1 && playAgain != 2)
                {
                    Console.WriteLine("Please enter a valid entry 1 to yes or 2 to quit !!");
                    playAgain = Convert.ToInt32(Console.ReadLine());
                }
                //Confirm quitting GameOver
                if (playAgain.Equals(2))
                    GameOver = true;
                //Otherwise go back to the Game
                else
                    
                    SelectNumber = rand.Next(1, 5);
            }
            
        }
    }
}
