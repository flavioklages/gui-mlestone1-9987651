﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,string> dictionary = new Dictionary<string, string>();
            dictionary.Add("banana", "fruit");
            dictionary.Add("carrot", "vegetable");
            dictionary.Add("apple", "fruit");
            dictionary.Add("pineaple","fruit");
            dictionary.Add("potato", "vegetable");
            dictionary.Add("parsnip", "vegetable");
            dictionary.Add("orange", "fruit");
            dictionary.Add("grapes", "fruit");
            dictionary.Add("tomato", "vegetable");
            dictionary.Add("pumpkin", "vegetable");


            Console.WriteLine("+----------------------------------------+");
            Console.WriteLine("|   Enter ether a fruit or a vegetable:  |");
            Console.WriteLine("+----------------------------------------+");
            
            string x = Console.ReadLine();
            string answer = "";
            if (dictionary.ContainsKey(x))
            {
                Console.WriteLine("Yes! the fruit is in the list");
                Console.ReadLine();
                /* answer = "Yes! the fruit is in the list";*/

            }
            else
            {
                Console.WriteLine("Nope! the fruit is not in the list");
                Console.ReadLine();

            }
            return answer;
            
        }
    }
}
