﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3_Console
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Console.WriteLine("+----------------------------------+");
            Console.WriteLine("|       Choose from the list :     |");
            Console.WriteLine("+----------------------------------+");
            Console.WriteLine("|                                  |");
            Console.WriteLine("|           1. Vegetables          |");
            Console.WriteLine("|           2. Fruits              |");
            Console.WriteLine("|           3. Exit                |");
            Console.WriteLine("+----------------------------------+");



           

                Console.WriteLine("+----------------------------------+");
                Console.Write("Please enter your selection: !\n");
                int selection = int.Parse(Console.ReadLine());
            
                if (selection == 1)
                {
                    var vegetables = new string[3] { "carrot", "potato", "parsnip" };

                    Console.WriteLine("Please choose one vegetable between: Carrot, Potato, Parsnip");
                    string choice = Console.ReadLine();
                    //Console.ReadKey();
                    if (choice == "carrot")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" A {vegetables[0]} is good for you!!");
                        Console.ReadKey();
                    }
                    else if (choice == "potato")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" A {vegetables[1]} is good for you!!");
                        Console.ReadKey();
                    }
                    else if (choice == "parsnip")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" A {vegetables[2]} is good for you!!");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine(" Please enter an item that is in the list !!");
                        Console.ReadKey();
                    }
                }

                if(selection==2)
                {
                    var fruit = new string[3] { "pineaple", "banana", "lemon" };

                    Console.WriteLine("Please choose one fruit between: pineaple, banana, lemon");
                    string choice = Console.ReadLine();
                    //Console.ReadKey();
                    if (choice == "pineaple")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" {fruit[0]} is very good for you and is yellow!!");
                        Console.ReadKey();
                    }
                    else if (choice == "banana")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" {fruit[1]} is good for you and very health!!");
                        Console.ReadKey();
                    }
                    else if (choice == "lemon")
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine($" {fruit[2]} is good for you and it has vitamin C!!");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("+------------------------------------+");
                        Console.WriteLine(" Please enter an item that is in the list !!");
                        Console.ReadKey();
                    }
                }
            

            Console.WriteLine("+------------------------------------+");
            Console.WriteLine(" Exiting program !!");
            Console.ReadKey();

        }
        }
    }

