﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task5_WindownsFormApp
{
    public partial class Form1 : Form
    {

        int number;
      
        int score = 0;
        public Form1()
        {
            InitializeComponent();
            //Generate the randow number
            Random generator = new Random();
            number = generator.Next(1, 6);
          

        }


        private void button1_Click(object sender, EventArgs e)
        {
            
            //get the user enter number
            int Usernumber = Convert.ToInt32(textBox1.Text);
            //int counter = 0;

            //check the Usernumber
            
            if(Usernumber > number)
            {
                label4Txt.Text = $"Too high. Try again !";
                label4Txt.Focus();
            }
            if(Usernumber < number)
            {
                label4Txt.Text = $"Too low. Try again!";
                label4Txt.Focus();

            }
            if(Usernumber == number)
            {
                
                MessageBox.Show("You got it!!!! \n   Do you want play new Game?\n\n\t(press new game) ");
               // btlEnter.Enabled = false;
            }
            if (Usernumber <= 0 || Usernumber >= 6)
            {
                label4Txt.Text = $"Please enter a valid number between 1 and 5 !";
            }

        }

        private void btlNewGame_Click(object sender, EventArgs e)
        {
            //InitializeComponent();
           // btlEnter.Enabled = true;
            textBox1.Clear();
            label4Txt.Text = "";
            Random generator = new Random();
            number = generator.Next(1, 6);
            
        }

        /*private void init()
        {
            Random generator = new Random();
            number = generator.Next(1, 6);
            //btlEnter.Enabled = true;
            //textBox1.Clear();
            //label4Txt.Text = " ";

        }*/
    }
}
