﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_5_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        int number;
        public MainPage()
        {
            this.InitializeComponent();
            //Generate the randow number
            Random generator = new Random();
            number = generator.Next(1, 6);

        }
        private void button1_Click(object sender, EventArgs e)
        {

            //get the user enter number
            int Usernumber = Convert.ToInt32(textBox1.Text);
            //int counter = 0;

            //check the Usernumber

            if (Usernumber > number)
            {
                label4Txt.Text = $"Too high. Try again !";
                label4Txt.Focus();
            }
            if (Usernumber < number)
            {
                label4Txt.Text = $"Too low. Try again!";
                label4Txt.Focus();

            }
            if (Usernumber == number)
            {

                MessageBox.Show("You got it!!!! \n   Do you want play new Game?\n\n\t(press new game) ");
                // btlEnter.Enabled = false;
            }
            if (Usernumber <= 0 || Usernumber >= 6)
            {
                label4Txt.Text = $"Please enter a valid number between 1 and 5 !";
            }

        }
        private void btlNewGame_Click(object sender, EventArgs e)
        {
            //InitializeComponent();
            // btlEnter.Enabled = true;
            textBox1.Clear();
            label4Txt.Text = "";
            Random generator = new Random();
            number = generator.Next(1, 6);

        }



    }
}
