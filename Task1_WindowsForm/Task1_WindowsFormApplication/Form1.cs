﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task1_WindowsFormApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btlKMText_Click(object sender, EventArgs e)
        {
            var convert = textBox1.Text;
            lblResultTxt.Text = $"{convert} Miles is {convertToMiles(convert)} Km's";
        }
        static double convertToMiles(string input)
        {
            const double Miles2Km = 1.609344;
            var userNum = double.Parse(input);
            var answer = userNum * Miles2Km;

            return answer;
        }

        private void btlMilesText_Click(object sender, EventArgs e)
        {
            var convert2 = textBox1.Text;
            lblResultTxt.Text = $"{convert2} Km's is {convertToKm(convert2)} Miles";
        }
        static double convertToKm(string input)
        {
            const double Km2Miles = 0.62137119;
            var userNum = double.Parse(input);
            var answer2 = userNum * Km2Miles;

            return answer2;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
